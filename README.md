PAN-OS Ansible Collection
=========

Ansible collection that automates the configuration and operational tasks on Palo Alto Networks Next Generation Firewalls, both physical and virtualized form factors, using the PAN-OS API.

Free software: Apache 2.0 License
Documentation: https://paloaltonetworks.github.io/pan-os-ansible/
PANW community supported live page: http://live.paloaltonetworks.com/ansible
Repo: https://github.com/PaloAltoNetworks/pan-os-ansible
Example Playbooks: https://github.com/PaloAltoNetworks/ansible-playbooks
Tested Ansible Versions
This collection is tested with the most current Ansible 2.9 and 2.10 releases. Ansible versions before 2.9.10 are not supported.

Installation
Install this collection using the Ansible Galaxy CLI:

ansible-galaxy collection install paloaltonetworks.panos

Support
This template/solution is released under an as-is, best effort, support policy. These scripts should be seen as community supported and Palo Alto Networks will contribute our expertise as and when possible. We do not provide technical support or help in using or troubleshooting the components of the project through our normal support options such as Palo Alto Networks support teams, or ASC (Authorized Support Centers) partners and backline support options. The underlying product used (the VM-Series firewall) by the scripts or templates are still supported, but the support is only for the product functionality and not for help in deploying or using the template or script itself.

Unless explicitly tagged, all projects or work posted in our GitHub repository (at https://github.com/PaloAltoNetworks) or sites other than our official Downloads page on https://support.paloaltonetworks.com are provided under the best effort policy.

Configuring Palo Alto for Automation
=========

**Once the firewall is up and running, change the admin password for the GUI**
- SSH to Palo alto as user admin
- Type “configure” and then “set mgt-config users admin password” enter new password twice and “commit” to save

**Create an administrator for api only access** <br>
https://www.youtube.com/watch?v=98w833RGqmA 


**Create a Custom execution environment for Palo Alto products** <br>
https://access.redhat.com/solutions/7030217 ( How to build an ee v3)
```
---
version: 3

dependencies:
  galaxy: requirements.yml
    
  python: 
    - pan-os-python
    - pan-python
    - panos-upgrade-assurance
    - xmltodict
  #system: []

options:
  package_manager_path: /usr/bin/microdnf

images:
  base_image:
    name: registry.redhat.io/ansible-automation-platform-24/ee-minimal-rhel8:latest
```

**Create a Custom Credential** <br>
Input Configuration:
```
fields:
  - id: api_key
    type: string
    label: Palo Alto api key
    secret: true
  - id: username
    type: string
    label: Username
  - id: password
    type: string
    label: Password
    secret: true
required:
  - api_key
  - username
  - password
```
Injector Configuration
```
extra_vars:
  api_key: '{{ api_key }}'
  username: '{{ username }}'
  password: '{{ password }}'
```

**Create an API Key** <br>
Paste into your browser the following with the correct username and password <br>
https://palo/api/?type=keygen&user=username&password=changeme <br>
Where "palo" is the dns name or ip address of your firewall

**Create a Panos/Palo Alto credential using the custom credential created earlier** <br>

**Rest API documentation** <br>
This can be found on the firewall at https://url_of_firewall/restapi-doc/